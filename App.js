import React from 'react';
import { View, Text, Button } from 'react-native';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

import LoginScreen from './src/screens/auth/login';
import SignupScreen from './src/screens/auth/signup';
import ForgetScreen from './src/screens/auth/forget';

import DashboardScreen from './src/screens/seller/dashboard';
import OrdersScreen from './src/screens/seller/orders';
import CatalogScreen from './src/screens/seller/catalog';
import CreateProductScreen from './src/screens/seller/create-product';
import SingleProductScreen from './src/screens/seller/product';
import SingleOrderScreen from './src/screens/seller/single-order';
import SellerRequestScreen from './src/screens/auth/seller-request';

const AuthStack = createStackNavigator({
  Login: LoginScreen,
  Signup: SignupScreen,
  Forget: ForgetScreen,
  SellerRequest: SellerRequestScreen
}, {
  initialRouteName: 'Login',
  mode: 'modal',
  headerMode: 'none'
});

const SellerStack = createStackNavigator({
  Dashboard: {
    screen: DashboardScreen
  },
  Orders: OrdersScreen,
  SingleOrder: SingleOrderScreen,
  Catalog: CatalogScreen,
  CreateProduct: {
    screen: CreateProductScreen
  },
  SingleProduct: SingleProductScreen,
}, {
  initialRouteName: 'Dashboard'
});

const AppNavigator = createStackNavigator({
  Auth: AuthStack,
  Seller: SellerStack
}, {
  headerMode: 'none'
});

export default createAppContainer(AppNavigator);
