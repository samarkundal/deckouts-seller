import React, { Component } from 'react';
import { Text, TouchableOpacity, View, StyleSheet } from 'react-native';

export default StyleSheet.create({
    ordersContainer: {

    },
    singleOrderContainer: {
        marginVertical: 0, 
        borderBottomWidth: 1, 
        paddingVertical: 8, 
        borderColor: '#eee',
        width: '100%'
    },
    orderTitle: {
        fontSize: 18, 
        fontWeight: '600', 
        color: '#3772d2'
    },
    orderSeperator: {
        backgroundColor: '#ccc', 
        padding: 2, 
        height: 5, 
        width: 5, 
        borderWidth: 1, 
        borderRadius: 40, 
        marginHorizontal: 8, 
        borderColor: '#ccc'
    },
    orderSubdetail: {
        color: '#888',
        textTransform: 'capitalize'
    },
    orderSubdetailContainer: {
        flexDirection: 'row', 
        justifyContent: 'flex-start', 
        alignContent: 'center', 
        alignItems: 'center',
        marginTop: 4
    }
});