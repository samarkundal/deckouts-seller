const Styles = {
    colors : {
        redDark: 'rgb(219, 48, 34)',
        redLight: '#f8d7da',
        redSemiLight: '#90484f',
        white: '#fff',
        greenLight: '#8be8e0',
        greenDark: '#1c6d66'
    }
}

export default Styles;