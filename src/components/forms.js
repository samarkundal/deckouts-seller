import React, { Component } from 'react';
import { Text, TouchableOpacity, View, StyleSheet } from 'react-native';

export default StyleSheet.create({
    input: { 
        borderColor: '#ddd', 
        borderWidth: 1, 
        padding: 16, 
        marginVertical: 8
    },
    textButton: {
        color: '#666', 
        fontSize: 16, 
        fontWeight: '500', 
        textTransform: 'capitalize',
        borderBottomWidth: 1,
        borderBottomColor: '#333',
        paddingBottom: 4
    }
});