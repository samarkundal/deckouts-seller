import React, { Component } from 'react';
import { Text, TouchableOpacity, View, StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    button: {
        padding: 16,
        backgroundColor: 'rgb(219, 48, 34)',
        alignItems: 'center',
        borderRadius: 48
    },
    buttonText: {
        color: '#fff',
        fontSize: 21
    },
    textButton: {
        color: '#666',
        fontSize: 16,
        fontWeight: '500',
        textTransform: 'capitalize',
        borderBottomWidth: 1,
        borderBottomColor: '#333'
    }
})

export class Button extends Component {

    constructor() {
        super();
    }

    render() {
        return (
            <View style={this.props.style}>
                <TouchableOpacity style={[styles.button, this.props.btnStyle]} onPress={this.props.onPress}>
                    <Text style={[styles.buttonText, this.props.btnTextStyle]}>{this.props.title}</Text>
                </TouchableOpacity>
            </View>
        );
    }
}

export class TextButton extends Component {

    constructor() {
        super();
    }

    render() {
        return (
            <View style={this.props.style}>
                <TouchableOpacity onPress={this.props.onPress}>
                    <Text style={styles.textButton}>{this.props.title}</Text>
                </TouchableOpacity>
            </View>
        );
    }
}