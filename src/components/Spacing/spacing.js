import { StyleSheet } from 'react-native';

const Spacing = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    auth: {
        padding: 16,
        marginTop: 84
    },
    loginHeading: {
        fontSize: 48,
        fontWeight: '700',
        marginVertical: 16
    }
});

export default Spacing;