import { AsyncStorage } from 'react-native';
const uuidv1 = require('uuid/v1');

var orders = [
    {
        _id: generateUid(),
        user: {
            name: 'Sahina Singh',
            email: 'sahina.singh@example.com',
        },
        orderId: 111,
        time: new Date(2019, 12, 12, 5, 45, 12),
        status: 'delivered',
        amount: {
            total: 1500,
            delivery: 50, 
            discount: 100,
            sum: 1350
        },
        items: [
            { 
                _id: uuidv1(),
                units: 2,
                name: 'Black Rajasthani Dress',
                amount: 300,
                total: 600
            }
        ],
        shippingAddress: 'Housing Road, Sector 22, Chandigarh',
    },
    {
        _id: generateUid(),
        user: {
            name: 'Mohit Marwar',
            email: 'mohit.marwar@example.com',
        },
        orderId: 112,
        time: new Date(2020, 2, 2, 5, 45, 12),
        status: 'delivered',
        amount: {
            total: 1800,
            delivery: 50, 
            discount: 100,
            sum: 1350
        },
        items: [
            { 
                _id: generateUid(),
                name: 'White Gujrati Dress',
                units: 2,
                amount: 300,
                total: 600
            },
            { 
                _id: generateUid(),
                name: 'Black Rajasthani Dress',
                units: 1,
                amount: 450,
                total: 450
            }
        ],
        shippingAddress: 'Housing Road, Sector 22, Chandigarh',
    }
]

const SessionService = {};

SessionService.getDashboard = function () {
    return new Promise((resolve, reject) => {
        var data = {
            "stats": {
                "totalProduct": 15,
                totalRevenue: 8500,
                weeklyRevenue: 4500,
                totalOrders: 8,
                weeklyOrders: 3,
                totalProducts: 5,
                newProducts: 2,
                "ordersCount": 0,
                "today": 0,
                "week": 0,
                "month": 0,
                "weeklyStats": {
                    "16-2-2020": 0,
                    "17-2-2020": 0,
                    "18-2-2020": 0,
                    "19-2-2020": 0,
                    "20-2-2020": 0,
                    "21-2-2020": 0,
                    "22-2-2020": 0
                }
            },
            "orders": orders
        }
        resolve(data);
    });
}

SessionService.getProducts = function() {

}

SessionService.createProduct = function () {

}

SessionService.getOrders = function () {

}

SessionService.createOrders = function () {

}

SessionService.setup = function () {
    return new Promise((resolve, reject) => {
        var data = {
            "stats": {
                "totalProduct": 15,
                "ordersCount": 0,
                "today": 0,
                "week": 0,
                "month": 0,
                "weeklyStats": {
                    "16-2-2020": 0,
                    "17-2-2020": 0,
                    "18-2-2020": 0,
                    "19-2-2020": 0,
                    "20-2-2020": 0,
                    "21-2-2020": 0,
                    "22-2-2020": 0
                }
            },
            "orders": orders
        }
        resolve({data});
    });
    
}

function generateUid() {
    return Math.random().toString(26).slice(2)
}

module.exports = SessionService;