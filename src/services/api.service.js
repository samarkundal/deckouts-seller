const axios = require('axios');
// const API = "http://192.168.0.55:3200/api/v1";
// const API = "http://deckouts.com/api/v1";
const API = "http://13.232.104.235:3200/api/v1";

import { AsyncStorage } from 'react-native';
const ApiService = {};

const KEYS = {
    tokenKey: '@deckoutSellers:token'
}


ApiService.loginUser = function (user) {
    return post('/sellers/login', user);
};

ApiService.getUserDashboard = async () => {
    return get('sellers/dashboard');
};

ApiService.sendSellerRequest = function(body) {
    return post('seller-requests', body);
}

ApiService.getUser = async () => {
    return get('sellers/dashboard');
};

ApiService.getProducts = async (productViewState) => {    
    var url = 'sellers/products';
    if(productViewState == 'available') {
        url = url + '/available';
    } else if(productViewState == 'out-of-stock'){
        url = url + '/out-of-stock';
    }
    return get(url);
}

ApiService.getProductColors = async () => {
    return get('sellers/products/colors');
}

ApiService.deleteProduct = async (productId) => {
    console.log('deleteConfirm', productId);
    return deleteRequest('sellers/products/' + productId);
}

ApiService.createProduct = async (product) => {
    console.log(product)
    var data = new FormData();
    var images = product.images.map((img) => {
        return {
            uri: Platform.OS === "android" ? img : img.replace("file://", ""),
            type: 'image/jpeg',
            name: Math.random() + '.jpg',
        }
    })
    delete product.images;
    Object.keys(product).forEach(key => {
        data.append(key, product[key]);
    });
    if(images && images.length) {
        data.append("images", images);
    }

    console.log(data);

    return new Promise(async (resolve, reject) => {
        var token = await ApiService.getToken();
        axios.default.post(`${API}/sellers/products/`, data, {
            headers: {
                'Content-Type' : 'multipart/form-data',
                'Authorization' : 'Bearer ' + token
            }
        }).then((result) => {
            console.log('result', result);
            resolve(result.data);
        }).catch((err) => {
            console.log(Object.keys(err));
            console.log(err.toJSON());
            console.log(err.response);
            console.log(err.request);
            console.log(err.config);
            console.log(err);
            reject(err.response);
        });
    });
}

ApiService.updateStock = async (productId, inStock) => {
    
    var data = new FormData();
    data.append('inStock', inStock);

    return new Promise(async (resolve, reject) => {
        var token = await ApiService.getToken();
        axios.default.post(`${API}/sellers/products/${productId}`, data, {
            headers: {
                'Content-Type' : 'multipart/form-data',
                'Authorization' : 'Bearer ' + token
            }
        }).then((result) => {
            resolve(result.data);
        }).catch((err) => {
            reject(err.response);
        });
    });
}

ApiService.updateProduct = async (productId, product) => {
    console.log(productId, product);
    var data = new FormData();
    var images = product.images.map((img) => {
        return {
            uri: Platform.OS === "android" ? img : img.replace("file://", ""),
            type: 'image/jpeg',
            name: Math.random() + '.jpg',
        }
    })
    
    delete product.images;
    Object.keys(product).forEach(key => {
        data.append(key, product[key]);
    });

    if(images && images.length) {
        data.append("images", images);
    }

    console.log(data);

    return new Promise(async (resolve, reject) => {
        var token = await ApiService.getToken();
        axios.default.post(`${API}/sellers/products/${productId}`, data, {
            headers: {
                'Content-Type' : 'multipart/form-data',
                'Authorization' : 'Bearer ' + token
            }
        }).then((result) => {
            resolve(result.data);
        }).catch((err) => {
            console.log(Object.keys(err));
            console.log(err.toJSON());
            console.log(err.response);
            console.log(err.request);
            console.log(err.config);
            reject(err.response);
        });
    });
}

ApiService.getCategories = async () => {
    return get('categories');
}

ApiService.getSingleProduct = async (productId) => {
    return get('products/' + productId);
}

ApiService.getOrders = async (timeFilter) => {
    var url = 'sellers/orders';
    if(timeFilter == 'last-7-days') {
        url = url + '/last-7-days';
    } else if(timeFilter == 'last-30-days'){
        url = url + '/last-30-days';
    }
    return get(url);
}


ApiService.getSingleOrder = async (orderId) => {
    return get('orders/' + orderId);
}

ApiService.updateOrderStatus = async (orderId, status) => {
    return post('sellers/orders/' + orderId + '/update-status', { status });
}

ApiService.getAccount = async () => {
    return get('sellers');
}

ApiService.updateAccount = async (sellerDetails) => {
    return post('sellers/update', sellerDetails);
}

ApiService.getToken = async () => {
    try {
        console.log('Get User', KEYS.tokenKey);
        var value = await AsyncStorage.getItem(KEYS.tokenKey);
        console.log('Value', value);

        if (value !== null) {
            return value;
        }
    } catch (error) {
        // Error saving data
    }
}

ApiService.setToken = async (user) => {
    try {
        await AsyncStorage.setItem(KEYS.tokenKey, user.token);
    } catch (error) {
        // Error saving data
    }
};

ApiService.deleteToken = async () => {
    try {
        await AsyncStorage.removeItem(KEYS.tokenKey);
    } catch (error) {
        
    }
}

function get(url, query) {
    return new Promise(async (resolve, reject) => {
        var token = await ApiService.getToken();
        console.log('GET: ', url, token);
        axios.default.get(`${API}/${url}`, {
            headers: {
                'Content-Type' : 'application/json',
                'Authorization' : 'Bearer ' + token
            }
        }).then((result) => {
            resolve(result.data);
        }).catch((err) => {
            reject(err.response);
        });
    });
}

function deleteRequest(url) {
    return new Promise(async (resolve, reject) => {
        var token = await ApiService.getToken();
        console.log('DELETE: ', url, token);
        axios.default.delete(`${API}/${url}`, {
            headers: {
                'Content-Type' : 'application/json',
                'Authorization' : 'Bearer ' + token
            }
        }).then((result) => {
            resolve(result.data);
        }).catch((err) => {
            reject(err.response);
        });
    });
}

function post (url, data) {
    return new Promise(async (resolve, reject) => {
        var token = await ApiService.getToken();
        console.log('POST :', url, token);
        axios.default.post(`${API}/${url}`, data, {
            headers: {
                'Content-Type' : 'application/json',
                'Authorization' : 'Bearer ' + token
            }
        }).then((result) => {
            resolve(result.data);
        }).catch((err) => {
            reject(err.response);
        });
    });
}

export default ApiService;