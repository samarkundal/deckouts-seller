import React, { Component } from 'react';
import { View, Text, Picker, Image, StyleSheet, Alert } from 'react-native';
import { Button } from '../../components/button';
import { ScrollView, State } from 'react-native-gesture-handler';
import ApiService from '../../services/api.service';
import { Icon } from 'react-native-elements';
import Styles from '../../components/styles';
import Loader from '../../components/loader';

const defaultImage = 'https://static8.depositphotos.com/1377527/931/i/950/depositphotos_9312172-stock-photo-little-girl-twirling.jpg';

var styles = StyleSheet.create({
    container: {
        height: '100%',
        backgroundColor: '#f5f6f7'
    },
    pricingContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingTop: 16,
        paddingBottom: 4
    },
    productTitle: {
        fontSize: 26,
        fontWeight: '600'
    },
    image: {
        width: '100%',
        height: 400
    },
    category: {
        color: '#4285f4',
        fontSize: 14,
        textTransform: 'capitalize',
        marginBottom: 8
    },
    price: {
        fontSize: 28,
        fontWeight: '600',
        marginTop: 10
    },
    productDesc: {
        fontSize: 16,
        lineHeight: 24,
        color: '#667',
        paddingBottom: 32,
        marginTop: 4
    },
    floatingButtons: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        flexGrow: 1,
        padding: 12,
        shadowOffset: {
            width: -5,
            height: -5,
        },
        backgroundColor: '#fff',
        shadowColor: '#000000',
        shadowOpacity: 0.8,
        zIndex: 9,
        elevation: 32,
        borderTopColor: '#ddd',
        borderTopWidth: 1
    },
    buttons: {
        paddingHorizontal: 16, 
        paddingVertical: 12
    }
});

export default class ProductScreen extends Component {
    static navigationOptions = {
        title: 'Product',
        headerStyle: {
            backgroundColor: '#f6f7f8',
        },
        headerTitleAlign: 'center',
        headerTitleStyle: {
            fontWeight: 'bold',
            color: '#222'
        }
    };

    constructor(props) {
        super(props);

        this.state = {
            product: {
                category: {}
            },
            inStock: true
        }
    }

    componentDidMount() {
        var productId = this.props.navigation.state.params.productId;
        console.log('productId', productId);
        ApiService.getSingleProduct(productId).then((result) => {
            console.log(result);
            this.setState({ product: result.data, loading: false, inStock: result.data.inStock });
        });

        const { navigation } = this.props;

        this.focusListener = navigation.addListener('didFocus', () => {            
            ApiService.getSingleProduct(productId).then((result) => {
                console.log(result);
                this.setState({ product: result.data, loading: false, inStock: result.data.inStock });
            });
        });
    }

    deleteConfirm() {
        var _this = this;
        Alert.alert(
            'Delete Product ?',
            'A product once deleted can\'t be retreived back.',
            [
                {
                    text: 'Cancel',
                    onPress: () => console.log('Cancel Pressed'),
                    style: 'cancel',
                },
                { text: 'Yes, Delete', onPress: () => _this.deleteProduct() },
            ],
            { cancelable: false },
        );
    }

    deleteProduct() {
        var productId = this.state.product._id;
        ApiService.deleteProduct(productId).then((result) => {
            console.log(result);
            this.props.navigation.navigate('Catalog');
        }).catch((err) => {
            console.log(err);
            alert(err);
        })
    }


    componentWillUnmount() {
        this.focusListener.remove();
    }

    pauseUnpauseOrders () {
        this.setState({ loading: true });
        ApiService.updateStock(this.state.product._id,  !this.state.inStock ).then((result) => {
            this.setState({ inStock: !this.state.inStock });
            this.setState({ loading: false });
        }).catch((err) => {
            console.log(err);
            alert(err);
        })
    }

    acceptPauseButtons() {
        if(this.state.inStock) {
            return <Button style={{ marginBottom: 32, width: 150 }} btnStyle={[styles.buttons, {backgroundColor: Styles.colors.redLight }]} btnTextStyle={{ color: Styles.colors.redSemiLight, fontSize: 14, paddingHorizontal: 8 }} title={'Out of Stock'} onPress={() =>  this.pauseUnpauseOrders() }></Button>
        } else {
            return <Button style={{ marginBottom: 32, width: 140 }} btnStyle={[styles.buttons, {backgroundColor: Styles.colors.greenLight }]} btnTextStyle={{ color: Styles.colors.greenDark, fontSize: 14, paddingHorizontal: 8 }} title={'Available'} onPress={() =>  this.pauseUnpauseOrders() }></Button>
        }
    }

    acceptPauseMessage() {
        if(this.state.inStock) {
            return <Text style={{fontSize: 16, fontWeight: '800', color: Styles.colors.greenDark, marginBottom: 16}}>Status: Accepting new Orders</Text>
        } else {
            return <Text style={{fontSize: 16, fontWeight: '800', color: Styles.colors.redDark, marginBottom: 16}}>Status: Not accepting new Orders</Text>;
        }
    }

    render() {
        var firstImage = defaultImage
        try {
            firstImage = this.state.product.images[0] || defaultImage;
        } catch (error) {
            
        }

        console.log('firstImage', firstImage);
        return (
            <View style={styles.container}>
                <Loader
                    loading={this.state.loading} />
                <ScrollView style={{ padding: 16, paddingBottom: 48 }} showsVerticalScrollIndicator={false}>
                    <View>
                        <Image style={styles.image} source={{ uri: firstImage }} />
                    </View>
                    <View>

                    </View>
                    <View style={styles.pricingContainer}>
                        <View>
                            <Text style={styles.productTitle}>{this.state.product.name}</Text>
                            <Text style={styles.seller}>{this.state.product.shortDesc}</Text>
                        </View>
                        <View>
                            <Text style={styles.price}>Rs {this.state.product.price}</Text>
                            {/* <Text style={styles.price}>Rs {this.state.product.purchasePrice}</Text> */}
                        </View>
                    </View>
                    <View>
                        <Text style={styles.category}>Category : {this.state.product.category.name}, Color : {this.state.product.color}</Text>
                        <Text style={styles.productDesc}>{this.state.product.productDesc}</Text>
                    </View>
                        <View style={{padding: 16, backgroundColor: '#fff', borderColor: '#ccc', borderWidth: 1}}>
                            <Text style={{fontSize: 18, marginBottom: 8}}>Update or Delete Product?</Text>
                            <Text style={{fontSize: 14, color: '#777', marginBottom: 16}}>A Product once deleted can't be reverted back.</Text>
                            <View style={{flexDirection: 'row'}}>
                                <Button style={{ marginBottom: 8, marginRight: 20 }} btnStyle={[styles.buttons, {backgroundColor: Styles.colors.redDark }]} btnTextStyle={{ color: Styles.colors.white, fontSize: 14, paddingHorizontal: 8 }} title={'Delete Product'} onPress={() => this.deleteConfirm() }></Button>
                                <Button style={{ marginBottom: 8 }} btnStyle={[styles.buttons, {backgroundColor: Styles.colors.redLight }]} btnTextStyle={{ color: Styles.colors.redSemiLight, fontSize: 14, paddingHorizontal: 8 }} title={'Edit Product'} onPress={() =>  this.props.navigation.navigate('CreateProduct', {
                                    productId: this.state.product._id
                                })}></Button>
                            </View>
                            <Text style={{fontSize: 18, marginBottom: 4, marginTop: 24}}>Update Product Availability ?</Text>
                            <Text style={{fontSize: 14, color: '#777', marginBottom: 8}}>If the product is not in stock, you can update the product to pause new orders.</Text>
                            { this.acceptPauseMessage() }
                            { this.acceptPauseButtons() }
                    </View>
                </ScrollView>
            </View>
        );
    }
}