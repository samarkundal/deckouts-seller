import React from 'react';
import { View, Text, Button, TextInput, TouchableOpacity, StyleSheet, SafeAreaView, FlatList } from 'react-native';
import Spacing from '../../components/Spacing/spacing';
import OrderStyles from '../../components/orders';
import ApiService from '../../services/api.service';
import SessionService from '../../services/session.service';
import { ScrollView } from 'react-native-gesture-handler';
import Loader from '../../components/loader';
import { Icon } from 'react-native-elements';

const styles = StyleSheet.create({
    sectionHeading: {
        fontSize: 32,
        fontWeight: '700'
    },
    statContainer: {
        padding: 16,
        alignItems: 'center',
        width: '48%',
        marginTop: '2%',
        marginBottom: '2%',
        backgroundColor: '#fff',
        borderWidth: 1,
        borderRadius: 5,
        borderColor: '#ddd'
    },
    statsHeading: {
        // color: '#272f38', 
        color: '#3772d2',
        fontSize: 24,
        fontWeight: '700',
        textTransform: 'capitalize'
    },
    statsLabel: {
        color: '#333',
        fontSize: 14,
        fontWeight: '400',
        color: '#678',
        textTransform: 'capitalize'
    }
});
let _this = null;
export default class DashboardScreen extends React.Component {
    static navigationOptions = {
        title: 'Dashboard',
        headerRight: () => (
            <TouchableOpacity style={{marginRight: 16, padding: 4, flexDirection: 'row', alignItems: 'center'}} onPress={_this.logoutUser}>
                <Icon
                    name='exclamation'
                    type='evilicon'
                    size={32}
                    style={{ width: 8, height: 8}} 
                    color='#517fa4' />
                <Text style={{marginLeft: 4, color: '#517fa4' }}>Logout</Text>
            </TouchableOpacity>
        )
    };


    logoutUser = async () => {
        console.log('Logout');
        await ApiService.deleteToken();
        this.props.navigation.navigate('Auth');
    }

    constructor(props) {
        super(props);
        
        _this = this;

        this.state = {
            stats: {},
            loading: true
        };

        ApiService.getUserDashboard().then((result) => {
            console.log(result);
            this.setState({
                totalProduct: result.totalProduct,
                stats: result.stats,
                orders: result.orders,
                loading: false
            });
        }).catch((err) => {
            this.setState({ loading: false });
            alert('Failed to authenticate the user.');
        })
    }

    renderItem = ({ item }) => {
        if(!item.user) {
            return;
        }
        return (
            <View style={OrderStyles.singleOrderContainer}>
                <TouchableOpacity activeOpacity={0.6} onPress={() => {
                    this.props.navigation.navigate('SingleOrder', {
                        orderId: item._id
                    })
                }}>
                    <View style={[OrderStyles.orderSubdetailContainer, { justifyContent: 'space-between' }]}>
                        <Text style={OrderStyles.orderTitle}>{item.user.name}</Text>
                        <Text style={{ fontSize: 18, color: '#3772d2', fontWeight: '700' }}>Rs {item.amount.total}</Text>
                    </View>
                    <View style={OrderStyles.orderSubdetailContainer}>
                        <Text style={OrderStyles.orderSubdetail}>#{item.orderId}</Text>
                        <View style={OrderStyles.orderSeperator}></View>
                        <Text style={OrderStyles.orderSubdetail}>Quantity: {item.items.length}</Text>
                        <View style={OrderStyles.orderSeperator}></View>
                        <Text style={OrderStyles.orderSubdetail}>{item.status}</Text>
                    </View>
                </TouchableOpacity>
            </View>

        );
    }

    render() {
        return (
            <ScrollView>
                <Loader
                    loading={this.state.loading} />
                <View style={[Spacing.container, { padding: 0, alignItems: 'flex-start', justifyContent: 'flex-start', backgroundColor: '#f1f2f3' }]}>
                    {/* <Text style={styles.sectionHeading}>Dashboard</Text> */}

                    <View style={{ alignItems: 'flex-start', padding: 16, display: 'flex', flexDirection: 'row', flexWrap: 'wrap' }}>
                        <TouchableOpacity style={[styles.statContainer, { marginRight: '2%' }]} onPress={() => this.props.navigation.navigate('Orders')}>
                            <Text style={styles.statsHeading}>Rs {this.state.stats.totalRevenue || '0'}</Text>
                            <Text style={styles.statsLabel}>Total Revenue</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={[styles.statContainer, { marginLeft: '2%' }]} onPress={() => this.props.navigation.navigate('Orders')}>
                            <Text style={styles.statsHeading}>Rs {this.state.stats.weeklyRevenue || '0'}</Text>
                            <Text style={styles.statsLabel}>Pending Payment</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={[styles.statContainer, { marginRight: '2%' }]} onPress={() => this.props.navigation.navigate('Orders')}>
                            <Text style={styles.statsHeading}>{this.state.stats.totalOrders || '0'}</Text>
                            <Text style={styles.statsLabel}>Total Orders</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={[styles.statContainer, { marginLeft: '2%' }]} onPress={() => this.props.navigation.navigate('Orders')}>
                            <Text style={styles.statsHeading}>{this.state.stats.pendingOrders || '0'}</Text>
                            <Text style={styles.statsLabel}>Pending Orders</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={[styles.statContainer, { marginRight: '2%' }]} onPress={() => this.props.navigation.navigate('Catalog')}>
                            <Text style={styles.statsHeading}>{this.state.stats.totalProducts || '0'}</Text>
                            <Text style={styles.statsLabel}>Total Products</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={[styles.statContainer, { marginLeft: '2%' }]} onPress={() => this.props.navigation.navigate('Catalog')}>
                            <Text style={styles.statsHeading}>{this.state.stats.availableProducts || '0'}</Text>
                            <Text style={styles.statsLabel}>Available Products</Text>
                        </TouchableOpacity>
                    </View>

                    <View style={[styles.container, { width: '100%', padding: 8 }]}>

                        <View style={[styles.statContainer, { width: '100%', alignItems: 'flex-start' }]}>
                            <View style={{ alignItems: 'flex-start', fontSize: 16, color: '#666', borderBottomColor: '#ddd', borderBottomWidth: 1, width: '100%', paddingBottom: 8 }}>
                                <Text style={{ fontSize: 16, color: '#666' }}>Pending Orders</Text>
                            </View>

                            <SafeAreaView style={styles.container}>
                                <FlatList
                                    showsVerticalScrollIndicator={true}
                                    data={this.state.orders}
                                    renderItem={this.renderItem}
                                    keyExtractor={item => item._id}
                                    numColumns={2}
                                />
                            </SafeAreaView>

                        </View>
                    </View>

                </View>
            </ScrollView>
        );
    }
}