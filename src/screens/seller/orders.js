import React, { Component } from 'react';
import { View, Text, SafeAreaView, FlatList, StyleSheet, Image, TouchableOpacity, Button } from 'react-native';
import OrderStyles from '../../components/orders';
import ApiService from '../../services/api.service';
import Loader from '../../components/loader';

const styles = StyleSheet.create({
    container: {
        padding: 16,
        backgroundColor: '#fff',
        marginTop: 16
    },
    item: {
        width: '50%',
        paddingVertical: 16,
        paddingHorizontal: 8
    },
    image: {
        width: '100%',
        minHeight: 200,
        borderRadius: 8
    },
    price: {
        color: 'rgb(219, 48, 34)',
        fontSize: 16
    },
    priceHolder: {
        paddingVertical: 8
    },
    seller: {
        color: '#789',
        fontSize: 11,
        fontWeight: '300'
    },
    title: {
        fontSize: 15,
        color: '#222',
        fontWeight: '700'
    },
    btnHolder: {
        flexDirection: 'row', 
        justifyContent: 'space-between', 
        padding: 16, 
        marginTop: 8
    }
});

const OrderViewStates = {
    AllOrders: 'all',
    Last7Days: 'last-7-days',
    Last30Days: 'last-30-days'
}

export default class OrdersScreen extends Component {
    static navigationOptions = {
        title: 'Order History',
        headerStyle: {
            backgroundColor: '#f6f7f8',
        },
        headerTitleAlign: 'center',
        headerTitleStyle: {
            fontWeight: 'bold',
            color: '#222'
        }
    };

    constructor(props) {
        super(props);
        
        this.state = {
            orders: {},
            loading: true,
            currentButton: OrderViewStates.AllOrders
        };

        ApiService.getOrders().then((result) => {
            console.log('Orders', result);
            var orders = result.data;
            this.setState({ orders, loading: false });
        });
    }

    componentDidMount() {
        const { navigation } = this.props;
        this.setState({ loading: true });
        this.focusListener = navigation.addListener('didFocus', () => {            
            ApiService.getOrders().then((result) => {
                var orders = result.data;
                this.setState({ orders, loading: false  });
            });
        });
    }

    componentWillUnmount() {
        this.focusListener.remove();
    }


    renderOrders = ({ item }) => {
        if(!item.user) {
            return;
        }
        return (
            <View style={OrderStyles.singleOrderContainer} key={item._id}>
                <TouchableOpacity activeOpacity={0.6} onPress={() => {
                    this.props.navigation.navigate('SingleOrder', {
                        orderId: item._id
                    })
                }}>
                    <View style={[OrderStyles.orderSubdetailContainer, { justifyContent: 'space-between' }]}>
                        <Text style={OrderStyles.orderTitle}>{item.user.name || ' '}</Text>
                        <Text style={{ fontSize: 18, color: '#3772d2', fontWeight: '700' }}>Rs {item.amount.total}</Text>
                    </View>
                    <View style={OrderStyles.orderSubdetailContainer}>
                        <Text style={OrderStyles.orderSubdetail}>#{item.orderId}</Text>
                        <View style={OrderStyles.orderSeperator}></View>
                        <Text style={OrderStyles.orderSubdetail}>Quantity: {item.items.length}</Text>
                        <View style={OrderStyles.orderSeperator}></View>
                        <Text style={OrderStyles.orderSubdetail}>{item.status}</Text>
                    </View>
                </TouchableOpacity>
            </View>
        );
    } 

    getSelectButtonColor(btnType) {
        console.log('getSelectButtonColor', btnType, this.state.currentButton);
        if(this.state.currentButton ==  btnType) {
            return 'rgb(219, 48, 34)';
        } else {
            return '#555';
        }
    }

    changeCurrentView(orderViewState){
        this.setState({ currentButton: orderViewState, loading: true });
        ApiService.getOrders(orderViewState).then((result) => {
            var orders = result.data;
            this.setState({ orders, loading: false });
        });
    }

    render() {
        return (
            <SafeAreaView>
                <View style={styles.btnHolder}>
                    <Button color={this.getSelectButtonColor(OrderViewStates.AllOrders)} onPress={() => this.changeCurrentView(OrderViewStates.AllOrders)} title={'All Orders'} />
                    <Button color={this.getSelectButtonColor(OrderViewStates.Last7Days)} onPress={() => this.changeCurrentView(OrderViewStates.Last7Days)} title={'Last 7 Days'} />
                    <Button color={this.getSelectButtonColor(OrderViewStates.Last30Days)} onPress={() => this.changeCurrentView(OrderViewStates.Last30Days)} title={'Last 30 Days'} />
                </View>
                <View style={styles.container}>
                    <Loader
                        loading={this.state.loading} />
                    <FlatList
                        showsVerticalScrollIndicator={false}
                        data={this.state.orders}
                        renderItem={this.renderOrders}
                        keyExtractor={item => item._id}
                        numColumns={2}
                        onPress={() => this.props.navigation.navigate('Product')}
                    />
                </View>
            </SafeAreaView>
        );
    };

}