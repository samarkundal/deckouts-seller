import React, { Component } from 'react';
import { View, Text, SafeAreaView, FlatList, StyleSheet, Image, TouchableOpacity, ScrollView } from 'react-native';
import OrderStyles from '../../components/orders';
import { Icon } from 'react-native-elements';
import ApiService from '../../services/api.service';
const defaultImage = 'https://static8.depositphotos.com/1377527/931/i/950/depositphotos_9312172-stock-photo-little-girl-twirling.jpg';
import Loader from '../../components/loader';
import Styles from '../../components/styles';

const products = [
    {
        id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
        title: 'Grey Dress Item',
        seller: 'John Hoppins',
        price: 400,
        quantity: 2
    },
    {
        id: '3ac68afc-c605-48d3-a4f8-fbd91aa97f61',
        title: 'Blue Second Item',
        seller: 'John Hoppins',
        price: 450,
        quantity: 2
    },
    {
        id: '3ac68afc-c605-48d3-a4f8-fbd91aa97h63',
        title: 'Green Second Item',
        seller: 'John Hoppins',
        price: 450,
        quantity: 4
    }
]

const styles = StyleSheet.create({
    orderStatusContainer: {
        padding: 0,
        marginBottom: 4,
        flexDirection: 'row',
        marginTop: 16
    },
    statusBlock: {
        alignItems: 'center',
        alignContent: 'center',
        width: '33%',
        backgroundColor: '#fff',
        paddingVertical: 16
    },
    statusText: {
        marginTop: 8
    },
    orderDetailsContainer: {
        padding: 16,
        backgroundColor: '#fff',
        marginTop: 16,
        borderWidth: 1,
        borderColor: '#ddd',
        flexDirection: 'row',
        flexWrap: 'wrap'
    },
    labelGroup: {
        width: '50%',
        marginBottom: 16
    },
    label: {
        fontSize: 14,
        color: '#777',
        marginBottom: 4
    },
    value: {
        fontSize: 16,

    },
    productsContainer: {
        padding: 8,
        backgroundColor: '#fff',
        borderColor: '#ddd',
        borderWidth: 1,
        marginTop: 8
    },
    productsHeading: {
        color: '#777',
        fontSize: 16,
        marginBottom: 8
    },
    products: {

    },
    
    productWrapper: {
        backgroundColor: '#fff',
        borderRadius: 8,
        elevation: 6,
        margin: 4,
        flexDirection: 'row',
        marginBottom: 16
    },
    productDesc: {
        padding: 16,
        flexGrow: 1
    },
    actions: {
        padding: 16,
        marginBottom: 32,
        flexDirection: 'row'
    }
});


class Button extends Component {

    constructor() {
        super();
    }

    render() {
        return (
            <TouchableOpacity style={[{ padding: 16, margin: 4, border: 1, backgroundColor: '#ddd', alignContent: 'center', alignItems: 'center', borderRadius: 40 }, this.props.style]} onPress={this.props.onPress}>
                <Text style={[{ fontSize: 16, fontWeight: '700'}, this.props.textStyle]}>{this.props.title}</Text>
            </TouchableOpacity>
        );
    }
}

export default class SingleOrder extends Component {

    static navigationOptions = {
        title: 'Order Details',
        headerStyle: {
            backgroundColor: '#f6f7f8',
        },
        headerTitleAlign: 'center',
        headerTitleStyle: {
            fontWeight: 'bold',
            color: '#222'
        }
    };

    constructor(props) {
        super(props);

        this.state = {
            order: {
                user: {},
                items: [],
                amount: {}
            },
            loading: true
        }

        var orderId = this.props.navigation.state.params.orderId;
        console.log('orderId', orderId);

        ApiService.getSingleOrder(orderId).then((result) => {
            console.log('order', result);
            this.setState({ order: result.data, loading: false});

        })
    }

    renderSingleProduct = (item) => {
        console.log('this', product);
        var product = item.product;
        var firstImage = defaultImage
        try {
            firstImage = product.images[0] || defaultImage;
        } catch (error) {
            
        }
        return (
            <View style={styles.productWrapper} key={product._id}>
                
                <View>
                    <Image
                        source={{ uri: 'https://static8.depositphotos.com/1377527/931/i/950/depositphotos_9312172-stock-photo-little-girl-twirling.jpg' }}
                        style={{ width: 140, height: 140 }}></Image>
                </View>
                <View style={styles.productDesc}>
                    <Text style={{ fontSize: 18, fontWeight: '700' }}>{product.name}</Text>
                    {/* <View style={{ flexDirection: 'row' }}>
                        <Text>Units: </Text>
                        <Text>{item.units}</Text>
                    </View> */}
                    <View style={{ flexDirection: 'row', justifyContent: "space-between", flexGrow: 1, marginTop: 4 }}>
                        <View style={{ flexDirection: 'row', width: 50 }}>
                            <Text># : {item.units}</Text>
                        </View>
                        <View style={{ alignItems: 'flex-end' }}>
                            <Text style={{ color: '#555' }}>Rs {product.price}/Item</Text>
                            <Text style={{ fontSize: 21, fontWeight: '700' }}>Rs {item.total}</Text>
                        </View>
                    </View>
                </View>
            </View>
        );
    }

    renderProducts = () => {
        console.log(products);
        var items = this.state.order.items;
        var productInside = [];
        for(var i = 0; i < items.length; i++) {
            productInside.push(this.renderSingleProduct(items[i]));
        }
        return productInside;
    }

    updateOrderStatus() {
        this.setState({ loading: true });
        ApiService.updateOrderStatus(this.state.order._id, 'delivered').then((result) => {
            console.log('re', result);
            this.setState({ loading: false, order: result });
        });
    }

    renderOrderDetails() {
        if(this.state.order.status == 'received') {
            return ([
                <View>
                    <Text style={{ color: '#555', lineHeight: 22, fontSize: 15 }}>If the order is ready for delivery, Please update the Order Status.</Text>
                    <View>
                        <Button title={'Out for Delivery'} style={{width: 200, padding: 12, marginTop: 16, backgroundColor: Styles.colors.greenLight }} textStyle={{fontSize: 16}} onPress={() => {this.updateOrderStatus()} }></Button>
                    </View>
                </View>
                ]);
        } else if(this.state.order.status == 'delivered') {
            return ([
                <View>
                    <Text style={{ color: '#555', lineHeight: 22, fontSize: 15 }}>The order is successfully sent for delivery.</Text>
                </View>
                ]);
        }
    }

    render() {
        return (
            <ScrollView style={{ padding: 8, paddingBottom: 50 }}>
                <Loader
                    loading={this.state.loading} />
                <View style={styles.orderStatusContainer}>
                    <View style={styles.statusBlock}>
                        <Icon
                            name='check'
                            type='evilicon'
                            size={40}
                            color='#517fa4'/>
                        <Text style={[styles.statusText, { color: '#517fa4'}]}>Received</Text>
                    </View>
                    <View style={[styles.statusBlock, { opacity: 0.5}]}>
                        <Icon
                            name='cart'
                            type='evilicon'
                            size={40}
                            color='#444' />
                        <Text style={styles.statusText}>Delivered</Text>
                    </View>
                    <View style={[styles.statusBlock, { opacity: 0.5}]}>
                        <Icon
                            name='like'
                            type='evilicon'
                            size={40}
                            color='#444' />
                        <Text style={styles.statusText}>Completed</Text>
                    </View>
                </View>

                <View style={styles.orderDetailsContainer}>
                    <View style={styles.labelGroup}>
                        <Text style={styles.label}>Order Id</Text>
                        <Text style={styles.value}>#{this.state.order.orderId}</Text>
                    </View>
                    <View style={styles.labelGroup}>
                        <Text style={styles.label}>Customer</Text>
                        <Text style={styles.value}>{this.state.order.user.name}</Text>
                    </View>
                    <View style={styles.labelGroup}>
                        <Text style={styles.label}>Amount</Text>
                        <Text style={styles.value}>Rs {this.state.order.amount.total}</Text>
                    </View>
                    <View style={[styles.labelGroup, { width: '100%' }]}>
                        <Text style={styles.label}>Shipping Address</Text>
                        <Text style={styles.value}>{this.state.order.shippingAddress}</Text>
                    </View>
                </View>

                <View style={styles.productsContainer}>
                    <Text style={styles.productsHeading}>Products</Text>
                    <View style={styles.products}>
                        {this.renderProducts()}
                    </View>
                </View>


                <View style={[styles.productsContainer, { marginBottom: 16, padding: 16}]}>
                    <Text style={styles.productsHeading}>Order Status</Text>
                    
                    {this.renderOrderDetails()}
                </View>

                {/* <View style={styles.actions}>
                    <Button style={{width: '50%'}} title={'Cancel Order'}></Button>
                    <Button style={{width: '50%', backgroundColor: 'green'}} textStyle={{color: '#fff'}} title={'Confirm Delivery'}></Button>
                </View> */}
            </ScrollView>
        );
    };

}