import React from 'react';
import { View, Text, SafeAreaView, ScrollView, TextInput, Image, TouchableOpacity, StyleSheet, Picker, CheckBox } from 'react-native';
import Spacing from '../../components/Spacing/spacing';
import { Button, TextButton } from '../../components/button';
import * as ImagePicker from 'expo-image-picker';
import ApiService from '../../services/api.service'
import Loader from '../../components/loader';

var styles = StyleSheet.create({
    container: {
        padding: 16,
        backgroundColor: '#fff',
        marginVertical: 16,
        marginHorizontal: 8,
        borderWidth: 1,
        borderColor: '#ddd'
    },
    formGroup: {
        marginVertical: 8
    },
    label: {
        marginBottom: 8,
        color: '#777'
    },
    input: {
        padding: 8,
        borderWidth: 1,
        borderColor: '#eee',
        backgroundColor: '#f1f2f3'
    },
    textarea: {
        padding: 8,
        borderWidth: 1,
        height: 150,
        borderColor: '#eee',
        textAlignVertical: "top",
        backgroundColor: '#f1f2f3'
    },
    imageHolder: {
        flexDirection: 'row',
    },
    image: {
        width: 60,
        height: 60,
        marginRight: 8
    },
    addImage: {
        backgroundColor: '#f1f2f3',
        justifyContent: 'center',
        alignItems: 'center',
        width: 60,
        height: 60,
        alignContent: 'center',
        textAlign: 'center'
    }
});

export default class CreateProductScreen extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            productId: null,
            productName: '',
            productCategory: '',
            productColor: '',
            images: [],
            productDesc: '',
            shortDesc: '',
            price: null,
            categories : [],
            purchasePrice: null,
            colors : [], 
            errors: null,
            loading: false
        }
    }

    static navigationOptions =  ({ navigation }) => {
        return {
            title: navigation.getParam('Title', 'Add a Product'),
        }
    };

    componentDidMount() {
        ApiService.getCategories().then((result) => {
            var categories = result.data;
            this.setState({ categories });
        });

        ApiService.getProductColors().then((result) => {
            console.log('Colors', result);
            var colors = result.data;
            this.setState({ colors });
        });

        if(this.props.navigation.state.params) {

            var productId = this.props.navigation.state.params.productId;
            if(productId) {
                this.setState({ loading: true });
                this.props.navigation.setParams({Title: 'Update Product'});
                
                ApiService.getSingleProduct(productId).then((result) => {
                    var product = result.data;
                    console.log(product);
                    this.setState({ price: String(product.price) });
                    this.setState({ purchasePrice: String(product.purchasePrice) });
                    this.setState({ shortDesc: product.shortDesc });
                    this.setState({ productDesc: product.productDesc });
                    this.setState({ productId: product._id, productCategory: product.category._id, productName: product.name, images: product.images, loading: false });
                    console.log(this.state);
                    this.setState({ loading: false });
                });
            }
        }
    }

    handleChoosePhoto = async () => {
        let result = await ImagePicker.launchImageLibraryAsync({
            mediaTypes: ImagePicker.MediaTypeOptions.All,
            allowsEditing: true,
            aspect: [4, 3],
            quality: 1
        });

        console.log(result);

        if (!result.cancelled) {
            var images = this.state.images;
            images.push(result.uri);
            this.setState({ images: images });
        }
    }

    renderImages() {
        var imagesViews = [];
        for(var i = 0; i < this.state.images.length; i++) {
            var image = this.state.images[i];
            imagesViews.push(<Image key={"image_" + i} style={styles.image} source={{ uri: image}} />);
        }
        return imagesViews;
    }

    addProduct() {
        console.log(this.state);
        var data = {
            name: this.state.productName,
            shortDesc: this.state.shortDesc,
            category: this.state.productCategory,
            images: this.state.images,
            productDesc: this.state.productDesc,
            color: this.state.productColor
        }
        if(this.state.price && this.state.price != '') {
            data.price = parseInt(this.state.price)
        }
        if(this.state.purchasePrice && this.state.purchasePrice != '') {
            data.purchasePrice = parseInt(this.state.purchasePrice)
        }
        this.setState({ loading: true });
        if(this.state.productId) {
            console.log(data);
            ApiService.updateProduct(this.state.productId, data).then((result) => {
                console.log('asdasdasd', result);
                this.setState({ loading: false });
                this.props.navigation.navigate('SingleProduct', {
                    productId: this.state.productId
                });
            }).catch((err) => {
                console.log('Error Update', err);
                var errors = err.data.errors;
                this.setState({ errors, loading: false });
                this.refs._scrollView.scrollTo({ y: 0});
            });
        } else {
            ApiService.createProduct(data).then((result) => {
                console.log(result);
                this.setState({ loading: false });
                this.props.navigation.navigate('Catalog');
            }).catch((err) => {
                console.log('Error Create', err);
                var errors = err.data.errors;
                this.setState({ errors, loading: false });
                this.refs._scrollView.scrollTo({ y: 0});
            });
        }
    }

    renderErrors(){
        console.log('Errors ', this.state.errors);
        var views = [];
        if(this.state.errors) {
            var errors = this.state.errors.map((error) => {
                views.push(<Text key={error.param} style={{color: '#90484f', paddingVertical: 2}}>{error.msg}</Text>);
            });
            return (
                <View style={{backgroundColor: '#f8d7da', padding: 16, marginBottom: 16}}>
                    {views}
                </View>
            );
        }
    }

    renderActionButton() {
        var act = (
            <View style={styles.formGroup}>
                <Button title="Create Product" style={{ marginTop: 8, padding: 6 }}  btnStyle={{padding: 12}} btnTextStyle={{fontSize: 18}} onPress={this.addProduct.bind(this)}></Button>
            </View>
        );
        if(this.state.productId) {
            act = (
                <View style={styles.formGroup}>
                    <Button title="Update Product" style={{ marginTop: 8, padding: 6 }} btnStyle={{padding: 12}} btnTextStyle={{fontSize: 18}} onPress={this.addProduct.bind(this)}></Button>
                </View>
            );
        }
        return act;
    }

    render() {
        let serviceItems = this.state.categories.map( (cat) => {
            return <Picker.Item key={cat._id} value={cat._id} label={cat.name} />
        });

        let getColors = this.state.colors.map( (color) => {
            return <Picker.Item key={color.label} value={color.label} label={color.value} />
        });


        return (
            <ScrollView style={{ padding: 8, paddingBottom: 50 }} ref='_scrollView'>
                <Loader
                    loading={this.state.loading} />
                <View style={styles.container}>
                    {this.renderErrors()}
                    <View style={styles.formGroup}>
                        <Text style={styles.label}>Product Name</Text>
                        <TextInput value={this.state.productName} style={styles.input} onChangeText={(productName) => this.setState({productName})}></TextInput>
                    </View>
                    <View style={styles.formGroup}>
                        <Text style={styles.label}>Product Category</Text>
                        <View styles={styles.input}>
                            <Picker
                                selectedValue={this.state.productCategory}
                                style={{backgroundColor:'#f1f2f3', height: 50 }}
                                onValueChange={(itemValue, itemIndex) =>
                                    this.setState({ productCategory: itemValue })
                                }>
                                {serviceItems}
                            </Picker>
                        </View>
                    </View>
                    <View style={styles.formGroup}>
                        <Text style={styles.label}>Product Color</Text>
                        <View styles={styles.input}>
                            <Picker
                                selectedValue={this.state.productColor}
                                style={{backgroundColor:'#f1f2f3', height: 50 }}
                                onValueChange={(itemValue, itemIndex) =>
                                    this.setState({ productColor: itemValue })
                                }>
                                {getColors}
                            </Picker>
                        </View>
                    </View>
                    <View style={styles.formGroup}>
                        <Text style={styles.label}>Rental Price</Text>
                        <TextInput value={this.state.price} style={styles.input} keyboardType={'numeric'} onChangeText={(price) => this.setState({price})}></TextInput>
                    </View>
                    <View style={styles.formGroup}>
                        <Text style={styles.label}>Purchase Price</Text>
                        <TextInput value={this.state.purchasePrice} style={styles.input} keyboardType={'numeric'} onChangeText={(purchasePrice) => this.setState({purchasePrice})}></TextInput>
                    </View>
                    <View style={styles.formGroup}>
                        <Text style={styles.label}>Short Description</Text>
                        <TextInput value={this.state.shortDesc} style={styles.input} onChangeText={(shortDesc) => this.setState({shortDesc})}></TextInput>
                    </View>
                    <View style={styles.formGroup}>
                        <Text style={styles.label}>Product Description</Text>
                        <TextInput value={this.state.productDesc} style={styles.textarea} multiline={true} numberOfLines={8} onChangeText={(productDesc) => this.setState({productDesc})}></TextInput>
                    </View>
                    <View style={styles.formGroup}>
                        <Text style={styles.label}>Product Images</Text>
                        <View style={styles.imageHolder}>
                            {this.renderImages()}
                            <TouchableOpacity style={styles.addImage} onPress={this.handleChoosePhoto}>
                                <Image style={{ width: 32, height: 32}} source={require('../../../assets/plus.png')} />
                            </TouchableOpacity>
                        </View>
                    </View>
                    
                    {this.renderActionButton()}

                </View>
            </ScrollView>
        );
    }
}