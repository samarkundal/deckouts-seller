import React, { Component } from 'react';
import { View, Text, SafeAreaView, FlatList, Button, StyleSheet, Image, TouchableOpacity } from 'react-native';
import ApiService from '../../services/api.service';
import Loader from '../../components/loader';
import { Icon } from 'react-native-elements';

const defaultImage = 'https://static8.depositphotos.com/1377527/931/i/950/depositphotos_9312172-stock-photo-little-girl-twirling.jpg';

const styles = StyleSheet.create({
    container: {
        padding: 0
    },
    item: {
        width: '50%',
        paddingVertical: 16,
        paddingHorizontal: 8
    },
    image: {
        width: '100%',
        minHeight: 200,
        borderRadius: 8
    },
    price: {
        color: 'rgb(219, 48, 34)',
        fontSize: 16
    },
    priceHolder: {
        paddingVertical: 8
    },
    seller: {
        color: '#789',
        fontSize: 11,
        fontWeight: '300'
    },
    title: {
        fontSize: 15,
        color: '#222',
        fontWeight: '700'
    },
    btnHolder: {
        flexDirection: 'row', 
        justifyContent: 'space-between', 
        padding: 16, 
        marginTop: 8
    }
});

const ProductViewStates = {
    All: 'all',
    Available: 'available',
    OutOfStock: 'out-of-stock'
}

export default class CatalogScreen extends Component {
    static navigationOptions =  ({ navigation }) => {
        return {
            title: 'Catalog',
            headerStyle: {
                backgroundColor: '#f6f7f8',
            },
            headerTitleAlign: 'center',
            headerTitleStyle: {
                fontWeight: 'bold',
                color: '#222'
            },
            headerRight: () => (
                <TouchableOpacity style={{marginRight: 16, padding: 4}} onPress={() => navigation.navigate('CreateProduct')}>
                    <Icon
                        name='plus'
                        type='evilicon'
                        size={40}
                        style={{ width: 16, height: 16}} 
                        color='#517fa4' />
                </TouchableOpacity>
            )
        }
    };

    constructor(props) {
        super(props);

        this.state = {
            stats: {
                loeading: true
            },
            products: [],
            currentButton: ProductViewStates.All
        };

        ApiService.getProducts().then((result) => {
            console.log('Products', result);
            var products = result;
            this.setState({ products, loading: false });
        });
    }

    renderProduct = ({ item }) => {
        var firstImage = defaultImage
        try {
            firstImage = item.images[0] || defaultImage;
        } catch (error) {
            
        }
        return (
            <View style={styles.item}>
                <TouchableOpacity activeOpacity={0.6} onPress={() => {
                        this.props.navigation.navigate('SingleProduct', {
                            productId: item._id
                        }) 
                    }}>
                    <Image style={styles.image} source={{ uri: firstImage }} />
                    <View style={styles.priceHolder}>
                        <Text style={styles.title}>{item.name}</Text>
                        <Text style={styles.price}>Rs {item.price}</Text>
                    </View>
                </TouchableOpacity>
            </View>
    
        );
    }

    componentDidMount() {
        const { navigation } = this.props;

        this.focusListener = navigation.addListener('didFocus', () => {            
            ApiService.getProducts().then((result) => {
                console.log('Products', result);
                var products = result;
                this.setState({ products });
            });
        });
    }

    componentWillUnmount() {
        this.focusListener.remove();
    }

    getSelectButtonColor(btnType) {
        if(this.state.currentButton ==  btnType) {
            return 'rgb(219, 48, 34)';
        } else {
            return '#555';
        }
    }

    changeCurrentView(productViewState){
        this.setState({ currentButton: productViewState, loading: true });
        ApiService.getProducts(productViewState).then((result) => {
            console.log('pr', result);
            var products = result;
            this.setState({ products, loading: false });
        });
    }

    render() {
        return (
            <SafeAreaView style={styles.container}>
                <Loader
                    loading={this.state.loading} />
                <View style={styles.btnHolder}>
                    <Button color={this.getSelectButtonColor(ProductViewStates.All)} onPress={() => this.changeCurrentView(ProductViewStates.All)} title={'All Products'} />
                    <Button color={this.getSelectButtonColor(ProductViewStates.Available)} onPress={() => this.changeCurrentView(ProductViewStates.Available)} title={'Available'} />
                    <Button color={this.getSelectButtonColor(ProductViewStates.OutOfStock)} onPress={() => this.changeCurrentView(ProductViewStates.OutOfStock)} title={'Out of Stock'} />
                </View>
                <FlatList
                    showsVerticalScrollIndicator={false}
                    data={this.state.products}
                    renderItem={this.renderProduct}
                    keyExtractor={item => item._id}
                    numColumns={2}
                    onPress={() => this.props.navigation.navigate('Product')}
                />
            </SafeAreaView>
        );
    };

}