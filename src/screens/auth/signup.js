import React from 'react';
import { View, Text, TextInput, TouchableOpacity, StyleSheet } from 'react-native';
import Spacing from '../../components/Spacing/spacing';
import { Button, TextButton } from '../../components/button';
import FormSyles from '../../components/forms';

export default class SignupScreen extends React.Component {
    static navigationOptions = {
        title: 'Signup',
    };

    createAccount = () => {
        alert('Signup');
    };

    render() {
        return (
            <View style={Spacing.auth}>
                <Text style={Spacing.loginHeading}>Signup</Text>
                
                <View>
                    <TextInput style={FormSyles.input} placeholder={'John Doe'}/>
                    <TextInput style={FormSyles.input} placeholder={'john@example.com'}></TextInput>
                    <TextInput style={FormSyles.input} placeholder={'********'}></TextInput>
                </View>

                <View style={{ alignItems: 'flex-end' }}>
                    <TouchableOpacity style={{ padding: 4 }} onPress={() => this.props.navigation.navigate('Forget')}><Text style={{ fontSize: 15, color: '#666' }}>Forget Password?</Text></TouchableOpacity>
                </View>

                <Button title="Create Account" style={{marginVertical: 8}} onPress={this.createAccount}></Button>

                <View style={{ alignItems: 'center', padding: 32 }}>
                    <Text style={{ color: '#999', marginBottom: 8 }}>OR</Text>
                    <TextButton title={'Login to your Account'} onPress={() => this.props.navigation.navigate('Login')}/>
                </View>
            </View>
        );
    }

    styles = StyleSheet.create({
        
    })
}