import React from 'react';
import { View, Text, Button, TextInput, TouchableOpacity } from 'react-native';
import Spacing from '../../components/Spacing/spacing';

export default class ForgetScreen extends React.Component {
    static navigationOptions = {
        title: 'Signup',
    };

    render() {
        return (
            <View style={Spacing.auth}>
                <Text style={Spacing.loginHeading}>Forget</Text>
                <View>
                    <Text style={{marginVertical: 16, color: '#678', lineHeight: 21}}>Please enter your email address. You will receive an email to reset the password via email.</Text>
                    <TextInput style={{borderColor: '#ddd', borderWidth: 1, padding: 16, marginVertical: 8}} placeholder={'john@example.com'}></TextInput>
                </View>
                <TouchableOpacity style={{padding: 16, backgroundColor: 'rgb(219, 48, 34)', alignItems: 'center', marginTop: 8}}>
                    <Text style={{color: '#fff', fontSize: 21, fontWeight: '500'}}>Reset Password</Text>
                </TouchableOpacity>

                <View style={{alignItems:'center', padding: 32}}>
                    {/* <Text style={{color: '#999', marginBottom: 8}}>OR</Text> */}
                    <TouchableOpacity style={{padding: 16, alignItems: 'center', marginTop: 8}} onPress={() => this.props.navigation.navigate('Login')}>
                        <Text style={{color: '#333', fontSize: 16, fontWeight: '700', textTransform:'capitalize'}}>Back to Login</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}