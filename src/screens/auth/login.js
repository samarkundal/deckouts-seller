import React from 'react';
import { View, Text, TextInput, TouchableOpacity } from 'react-native';
import { Button, TextButton } from '../../components/button';
import styles from '../../components/forms';
import Spacing from '../../components/Spacing/spacing';
import apiService from '../../services/api.service';
import Loader from '../../components/loader';
import { Icon } from 'react-native-elements';

export default class LoginScreen extends React.Component {
    static navigationOptions = {
        title: 'Login'
    };

    constructor(props) {
        super(props);

        console.log('Call apiService user');
        apiService.getUser().then((result) => {
            console.log('In login Constructor', result);
            if(result) {
                this.props.navigation.navigate('Seller');
            }
        }).catch(async (err) => {
            await apiService.deleteToken();
        });

        this.state = {
            email: null,
            password: null,
            error: null,
            loading: false,
            passwordSecure: true
        }
    }

    componentDidMount() {
        const { navigation } = this.props;
        this.setState({ loading: true });
        apiService.getUser().then((result) => {
            if(result) {
                this.props.navigation.navigate('Seller');
                this.setState({ loading: false });
            }
        }).catch(async (err) => {
            this.setState({ loading: false });
            await apiService.deleteToken();
        });
    }

    loginToAccount = () => {
        var self = this;
        var data = {
            email: this.state.email,
            password: this.state.password
        }
        var error = false;
        if(!this.state.email) {
            this.setState({ error: "Email is a required field."});
            error = true;
        } else {
            this.setState({ error: null });
        }
        if(!error && !this.state.password) {
            this.setState({ error: "Password is a required field."});
            error = true;
        } else if(!error){
            this.setState({ error: null });
        }
        if(!error) {
            this.setState({ loading: true });
            apiService.loginUser(data).then((result) => {
                console.log(result);
                if(result.error) {
                    this.setState({ error: "Invalid Username/Password Combination."});
                }
                if(!result.error) {
                    var seller = result.data;
                    console.log(seller);
                    apiService.setToken(seller);
                    
                    var val = apiService.getUser().then((res) => {
                        console.log('asdasd', res);
                        self.props.navigation.navigate('Seller');
                    })
                    console.log('asdasd', val);
                }
                this.setState({ loading: false });
            }).catch((err) => {
                console.log('err', err);
                this.setState({ error: "Invalid Username/Password Combination."});
            });
        }
    };

    eyeStyles() {
        if(this.state.passwordSecure) {
            return {
                opacity: 1
            }
        } else {
            return {
                opacity: 0.5
            }
        }
    }

    render() {
        return (
            <View style={Spacing.auth}>
                <Loader
                    loading={this.state.loading} />
                <Text style={Spacing.loginHeading}>Login</Text>

                {this.state.error &&
                    <View style={{backgroundColor: '#f8d7da', padding: 12, borderRadius: 5, marginBottom: 16, borderColor: '#f5c6cb', borderWidth: 1}}>
                        <Text style={{fontSize: 15, color: '#721c24'}}>{this.state.error}</Text>
                    </View>
                }

                <View style={{position: 'relative'}}>
                    <TextInput style={styles.input} placeholder={'john@example.com'}  onChangeText={(email) => this.setState({email})}></TextInput>
                    <TextInput style={styles.input} secureTextEntry={this.state.passwordSecure} placeholder={'********'}  onChangeText={(password) => this.setState({password})}></TextInput>
                    <View style={{ position: 'absolute', right: 10, bottom: 30}}>
                        <TouchableOpacity style={this.eyeStyles()} onPress={() => { this.setState({ passwordSecure: !this.state.passwordSecure }) }}>
                            <Icon
                                name='eye'
                                type='evilicon'
                                size={24}
                                color='#517fa4'/>
                        </TouchableOpacity>
                    </View>
                </View>

                <View style={{ alignItems: 'flex-end' }}>
                    <TouchableOpacity style={{ padding: 4 }} onPress={() => this.props.navigation.navigate('Forget')}><Text style={{ fontSize: 15, color: '#666' }}>Forget Password?</Text></TouchableOpacity>
                </View>

                <Button title="Login" style={{ marginTop: 8 }} onPress={this.loginToAccount}></Button>

                
                <View style={{alignItems:'center', padding: 32}}>
                    <Text style={{color: '#999', marginBottom: 8}}>Become a Seller</Text>
                    <TouchableOpacity style={{padding: 16, alignItems: 'center', marginTop: 8}} onPress={() => this.props.navigation.navigate('SellerRequest')}>
                        <Text style={{color: '#333', fontSize: 16, fontWeight: '700', textTransform:'capitalize'}}>Apply Now</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}