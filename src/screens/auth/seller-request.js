import React from 'react';
import { View, ScrollView, Text, Button, TextInput, TouchableOpacity, StyleSheet, Alert, KeyboardAvoidingView } from 'react-native';
import Spacing from '../../components/Spacing/spacing';
import Loader from '../../components/loader';
import ApiService from '../../services/api.service';

const styles = StyleSheet.create({
    formGroup: {
        marginBottom: 16
    },
    label: {
        fontSize: 14,
        marginBottom: 8
    },
    formControl: {
        borderColor: '#ddd',
        borderWidth: 1,
        padding: 12
    }
});

export default class SellerRequestScreen extends React.Component {
    static navigationOptions = {
        title: 'Signup',
    };

    constructor(props) {
        super(props);

        this.state = {
            productId: null,
            errors: null,
            loading: false,
            name: null,
            phone: null,
            email: null,
            company: null,
            gst: null,
            pancard: null,
            businessYear: null
        }
    }


    renderErrors() {
        var views = [];
        if (this.state.errors) {
            var errors = this.state.errors.map((error) => {
                views.push(<Text key={error.param} style={{ color: '#90484f', paddingVertical: 2 }}>{error.msg}</Text>);
            });
            return (
                <View style={{ backgroundColor: '#f8d7da', padding: 16, marginBottom: 16, marginTop: 16 }}>
                    {views}
                </View>
            );
        }
    }

    applySellerRequest() {
        console.log('Seller R');
        var data = {
            name: this.state.name,
            email: this.state.email,
            phone: this.state.phone,
            company: this.state.company,
            gst: this.state.gst,
            businessYear: this.state.businessYear,
            pancard: this.state.pancard
        };
        this.setState({ loading: true });
        var _this = this;
        ApiService.sendSellerRequest(data).then((result) => {
            console.log(result);
            this.setState({ loading: false });
            Alert.alert(
                'Your request is received',
                'Thanks for applying for DeckOuts Seller. Our team will get back to you.',
                [
                    { text: 'OK', onPress: () => _this.props.navigation.navigate('Login') },
                ],
                { cancelable: false },
            );
        }).catch((err) => {
            console.log(err);
            var errors = err.data.errors;
            console.log(errors);
            this.setState({ errors, loading: false });
            this.refs._scrollView.scrollTo({ y: 0 });
        })
    };

    render() {
        return (
            <KeyboardAvoidingView behavior={'padding'}>

                <ScrollView style={{ padding: 8, marginVertical: 32 }}>
                    <Loader
                        loading={this.state.loading} />
                    <Text style={[Spacing.loginHeading, { fontSize: 24 }]}>Apply to Become Seller</Text>
                    <View>
                        <Text style={{ marginVertical: 4, color: '#678', lineHeight: 21 }}>Please enter your email address. You will receive an email to reset the password via email.</Text>
                    </View>
                    {this.renderErrors()}
                    <View style={{ padding: 16, backgroundColor: '#fff', borderColor: '#ddd', borderWidth: 1, marginTop: 16 }}>
                        <View style={styles.formGroup}>
                            <Text style={styles.label}>Name</Text>
                            <TextInput style={styles.formControl} placeholder={'John Doe'} onChangeText={(name) => this.setState({ name })}></TextInput>
                        </View>
                        <View style={styles.formGroup}>
                            <Text style={styles.label}>Email</Text>
                            <TextInput style={styles.formControl} placeholder={'john@example.com'} onChangeText={(email) => this.setState({ email })}></TextInput>
                        </View>
                        <View style={styles.formGroup}>
                            <Text style={styles.label}>Phone Number</Text>
                            <TextInput style={styles.formControl} onChangeText={(phone) => this.setState({ phone })}></TextInput>
                        </View>
                        <View style={styles.formGroup}>
                            <Text style={styles.label}>Company/ Business Name</Text>
                            <TextInput style={styles.formControl} onChangeText={(company) => this.setState({ company })}></TextInput>
                        </View>
                        <View style={styles.formGroup}>
                            <Text style={styles.label}>PAN Details</Text>
                            <TextInput style={styles.formControl} onChangeText={(pancard) => this.setState({ pancard })}></TextInput>
                        </View>
                        <View style={styles.formGroup}>
                            <Text style={styles.label}>GST Number (Optional)</Text>
                            <TextInput style={styles.formControl} onChangeText={(gst) => this.setState({ gst })}></TextInput>
                        </View>
                        <View style={styles.formGroup}>
                            <Text style={styles.label}>Business Starting Year</Text>
                            <TextInput style={styles.formControl} keyboardType={'numeric'} onChangeText={(businessYear) => this.setState({ businessYear })}></TextInput>
                        </View>
                        <TouchableOpacity style={{ padding: 16, backgroundColor: 'rgb(219, 48, 34)', alignItems: 'center', marginTop: 8 }} onPress={this.applySellerRequest.bind(this)}>
                            <Text style={{ color: '#fff', fontSize: 18, fontWeight: '500' }}>Apply Now</Text>
                        </TouchableOpacity>
                    </View>

                    <View style={{ alignItems: 'center' }}>
                        {/* <Text style={{color: '#999', marginBottom: 8}}>OR</Text> */}
                        <TouchableOpacity style={{ padding: 16, alignItems: 'center', marginTop: 8 }} onPress={() => this.props.navigation.navigate('Login')}>
                            <Text style={{ color: '#333', fontSize: 16, fontWeight: '700', textTransform: 'capitalize' }}>Back to Login</Text>
                        </TouchableOpacity>
                    </View>
                </ScrollView>
            </KeyboardAvoidingView>
        );
    }
}